export class Creature {
    ///id: string = (Math.random() * Date.now()).toString();
    toString(): string {
        return `Creature`;
    }
}

export class Animal extends Creature {
    constructor(public name: string) {
        super();
    }
}

export class Dog extends Animal {
    constructor(name?: string) {
        super(name || 'A dog');
    }

    bark(): void {
        console.log(`${this.name}: Wrrrraaaath!`);
    }
}

export class Cat extends Animal {
    constructor(name?: string) {
        super(name || 'A cat');
    }

    meow(): void {
        console.log(`${this.name}: Meeeeeooooooow!`);
    };
}

export class Hamster extends Animal {
    constructor(name?: string) {
        super(name || 'A hamster');
    }

    boo(): void {
        console.log(`${this.name}: Booooooooo!`);
    }
}