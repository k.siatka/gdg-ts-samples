import { Animal, Cat, Dog, Hamster } from './creatures';

// String literal
type Tags = "<a>" | "<body>" | "<p>" | "<strong>";
let anchor: Tags = "<a>";
///let div: Tags = "<div>";

// Keyof (index type operator)
type A = keyof Animal; // "name", "toString"

let a1: A = "name";
///let a2: A = "meow";

type B = keyof A; // keyof T is a subtype of String
type S = keyof String;

let b1: B = "charAt";
let b2: B = "length";

let dogProps: keyof Dog; // "bark", "name", "toString"

function getCatProp(prop: keyof Cat) {
    console.log(prop);
}

getCatProp("meow");
getCatProp("name");
///getCatProp("bark"); // no bark in Cat


// keyof typeof
let n = 1;
let puppy = new Dog();
type N = keyof typeof n;
type D = keyof typeof puppy;


// Lookup (index access operator)
type T1 = Animal["name"]; // string
type T2 = Cat["meow"]; // () => void
type T3 = string[]["push"]; // (...items: string[]) => number

let pusher: T3 = (...args: string[]) => args.length;

// Keyof with generic type
function getTProp<T>(prop: keyof T) {
}

getTProp<Dog>("bark");
getTProp<Cat>("meow");


// Keyof with generic constarint and indexed access operator
function getProperty<T, K extends keyof T>(o: T, name: K): T[K] {
    return o[name]; // o[name] is of type T[K]
}

function getProperties<T, K extends keyof T>(o: T, names: K[]): Array<T[K]> {
    return names.map(n => getProperty(o, n));
}

getProperties(new Cat("Kitten"), ["name", "meow"]).forEach(
    p => console.log(`Property: ${p} of type: ${typeof p}`)
);


// Mapped Type
type Partial<T> = {
    [P in keyof T]?: T[P];
}

///let dog: Dog = { name: "dog barking" };
let dogNotBarking: Partial<Dog> = { name: "dog not barking" };

type Pick<T, K extends keyof T> = {
    [P in K]: T[P];
}
type AnimalSounds = Pick<Dog & Cat & Hamster, "bark" | "meow" | "boo">;

let creature: AnimalSounds = {
    bark: () => { },
    meow: () => { },
    boo: () => { }
};


type Record<K extends string, T> = {
    [P in K]: T;
}
type AnimalSoundDescriptions = Record<"bark" | "meow" | "boo", string>;

//Partial, Pick, Record are included in TypeScript’s standard library


function enumFromStrings<T extends string>(...o: T[]): {[P in T]: P} {
    let result = <{[P in T]: P}>{};
    o.forEach(key => result[key] = key);
    return result;
}

const Direction = enumFromStrings('Up', 'Down', 'Left', 'Right');
type Direction = keyof typeof Direction;

let goto: Direction = Direction.Up;
goto = "Left";
///goto = "Somewhere"
