import { Animal, Dog, Cat } from './creatures';

// Tuple
let a: [number, string] = [1, 'A'];
///let b: [number, string] = ['b', 'B']; // wrong first type

type MyTuple = [number, string, Animal];
let tuple: MyTuple = [1, 'Some string', new Dog()];
let arrayOfTuples: MyTuple[] = [tuple];
let tupleOfTuples: [MyTuple, MyTuple] = [tuple, tuple];

// Genric Tuple
export interface IKeyValuePair<TKey, TValue> extends Array<TKey | TValue> {
    0: TKey;
    1: TValue;
}

let kvp1: IKeyValuePair<number, Animal> = [1, new Animal('')];
let kvp2: IKeyValuePair<string, Animal> = ['pitbull', new Dog()];