import { Animal, Dog, Cat } from './creatures';

// Generic interface
export interface IDictionary<TKey, TValue> {
    count(): number;
    get(key: TKey): TValue;
    add(key: TKey, value: TValue): void;
    containsKey(key: TKey): boolean;
    remove(key: TKey): void;
    clear(): void;
    keys(): Array<TKey>;
    values(): Array<TValue>;
}


// Generic class
export class Dictionary<TKey, TValue> implements IDictionary<TKey, TValue> {
    private _keys: Array<TKey> = [];
    private _values: Array<TValue> = [];

    count(): number {
        return this._keys.length;
    }

    get(key: TKey): TValue {
        return this._values[this._keys.indexOf(key)];
    }

    clear(): void {
        this._keys.length = 0;
        this._values.length = 0;
    }

    add(key: TKey, value: TValue): void {
        this._keys.push(key);
        this._values.push(value);
    }

    containsKey(key: TKey): boolean {
        return this._keys.indexOf(key, 0) > 0;
    }

    remove(key: TKey): void {
        let index = this._keys.indexOf(key, 0);
        this._keys.splice(index, 1);
        this._values.splice(index, 1);
    }

    keys(): Array<TKey> {
        return this._keys;
    }

    values(): Array<TValue> {
        return this._values;
    }
}

let dict = new Dictionary<string, Animal>();
dict.add('puppy1', new Dog('puppy1'));
dict.add('puppy2', new Dog('puppy2'));
dict.add('kitten', new Cat('kitten'));
///dict.add(1, new Animal('')); // wrong key type
///dict.add('an animal', new Date()); // wrong value type

export class AnimalDictionary extends Dictionary<string, Animal> {
    addAnimal(animal: Animal): void {
        this.add(animal.name, animal);
    }  
}

let animalsDict = new AnimalDictionary();
animalsDict.addAnimal(new Dog('puppy1'));
animalsDict.addAnimal(new Dog('puppy2'));
animalsDict.addAnimal(new Cat('kitten'));


// Generic constraints
interface IName {
    name: string;
}

class NameDictionary<K extends IName, V> extends Dictionary<K, V> {
    getByName(name: string): V | never {
        let key = this.keys().find(k => k.name === name)
        if (key === undefined) {
            throw new Error(`Key ${key} is not found in dictionary.`);
        }
        return this.get(key);
    }
}


// Generic constraint can be generic ;)
interface IId<T> {
    id: T;
}

class IdDictionary<K extends IId<KC>, V, KC> extends Dictionary<K, V> {
    getById(id: KC): V | never {
        let key = this.keys().find(k => k.id === id)
        if (key === undefined) {
            throw new Error(`Key ${key} is not found in dictionary.`);
        }
        return this.get(key);
    }
}

class Employee implements IId<string> {
    id: string;
}

class Report {
}

let employeeReports = new IdDictionary<Employee, Report[], string>();


// Generic factory
function create<T>(c: {new(): T; }): T {
    return new c();
}

let manufacturedEmployee = create(Employee); // has parameterless constructor
///let manufaturedAnimal = create(Animal); // hasn't got parameterless constructor
let manufaturedCat = create(Cat); // has got constructor with optional argument


function createAnimal<A extends Animal>(c: new (name: string) => A, name: string): A {
    return new c(name);
}

let manufacturedDog = createAnimal(Dog, 'Manufactured dog');
manufacturedDog.bark();

class Snake extends Animal {
    constructor(l: number) {
        super(`Snake of lenght ${l}`)
    }
}

class PseudoAnimal {
    constructor(name: string) {
    }
}

///let manufacturedSanke = createAnimal(Snake, 'Manufactured snake'); // no constructor (name: string)
///let manufacturedPseudoAnimal = createAnimal(PseudoAnimal, 'Manufactured animal'); // not Animal