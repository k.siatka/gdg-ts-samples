import { Creature, Animal, Dog, Cat } from "./creatures";

function setAnimal(animal: Animal): void {
    console.log(animal.name);
}


let alien = new Creature();
let bird = new Animal("Parrot");
let puppy = new Dog("beagle");
let kitten = new Cat("boniek");

setAnimal(puppy);
setAnimal(kitten);
setAnimal(bird);
///setAnimal(alien);


function makeLowerCase(s: string): string {
    return s.toLowerCase();
}

function getLength(s: string): number {
    return s.length;
}

type MyPromise = Promise<string | number>;
let foo: MyPromise;
//declare let foo: Promise<string | number>;
///foo.then(makeLowerCase) // Error
///foo.then(getLength); // Error

let f1: (x: Animal) => void;
let f2: (x: Dog) => void;
let f3: (x: Cat) => void;
///f1 = f2;  // Error with --strictFunctionTypes
f2 = f1;  // Ok
///f2 = f3;  // Error
