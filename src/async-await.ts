import { Creature, Animal, Dog, Cat, Hamster } from "./creatures";
import fetch from "node-fetch";

const ANIMALS: Animal[] = [
    new Dog('pitbull'),
    new Dog('puddle'),
    new Dog('beagle'),
    new Cat('persian'),
    new Cat('mainecoon'),
    new Dog('sphinx'),
    new Hamster('minsc hamster')
]

// async await
function getAnimalsByName(name: string): Promise<Animal[]> {
    let p: Promise<Animal[]> = new Promise((resolve, reject) => {
        setTimeout(() => {
            if (name === "err") {
                reject("Somethig terrible happened!");
            }
            let found: Animal[] = ANIMALS.filter(a => a.name.indexOf(name) > -1);
            resolve(found);
        }, 2000); // 2s delay
    });
    return p;
}

async function logSearchResults(name: string) {
    let found = await getAnimalsByName(name);
    console.log(found);
}


async function fetchAsync(): Promise<string[]> {
    let req = await fetch('https://jsonplaceholder.typicode.com/posts');
    let json = await req.json();
    let data: string[] = json.map((o: any) => `${o.title} ${o.body}`);
    return data;
}


console.log('Beginning search...');

logSearchResults('m');

logSearchResults('err')
    .catch(reason => console.log(reason));

(async () => {
    let results = await fetchAsync();
    results.forEach(r => console.log(r));
})();

console.log('Search submitted...');