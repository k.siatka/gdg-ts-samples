// declare var foo: any;
///foo = 123;

interface ErrorX {
    message: string;
}

interface ErrorXConstructor {
    new(message: string): ErrorX;
    (message: string): ErrorX;
    readonly prototype: ErrorX;
}

declare const ErrorX: ErrorXConstructor;
