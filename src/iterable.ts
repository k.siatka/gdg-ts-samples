import { Dictionary } from './generics';
import { IKeyValuePair } from './tuples';
import { Animal, Dog, Cat } from './creatures';

// Iterable generic dictionary
export class IterableDictionary<K, V> extends Dictionary<K, V> implements Iterable<IKeyValuePair<K, V>> {
    [Symbol.iterator](): Iterator<IKeyValuePair<K, V>> {
        let counter = 0;
        let keys = this.keys();
        let values = this.values();

        return {
            next(): IteratorResult<IKeyValuePair<K, V>> {
                if (counter < keys.length) {
                    let yeildResult = {
                        done: false,
                        value: <IKeyValuePair<K, V>>[keys[counter], values[counter]]
                    }
                    counter++;
                    return yeildResult;
                } else {
                    return {
                        done: true,
                        value: null
                    }
                }
            }
        }
    }
}

let animals = new IterableDictionary<string, Animal>();
animals.add('puppy1', new Dog('puppy1'));
animals.add('puppy2', new Dog('puppy2'));
animals.add('kitten', new Cat('kitten'));

console.log(`Animals: ${animals.count()}`);

for (let kvp of animals) {
    console.log(kvp[0], kvp[1])
}