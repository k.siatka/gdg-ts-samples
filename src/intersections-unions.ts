import { Creature, Animal, Dog, Cat, Hamster } from "./creatures";

// Intersection types
let hybrid0: Dog & Cat;
hybrid0.bark();
hybrid0.meow();

function hybridAnd(input: Dog & Cat & Hamster): void {
    // every prop available here
    input.bark();
    input.meow();
    input.boo();
}

hybridAnd({
    name: '',
    bark: () => {},
    meow: () => {},
    boo: () => {}
});

function extend<T, U>(first: T, second: U): T & U {
    // some deep extending goes here
    return <T & U>{};
}

let hybrid1: Dog & Cat = extend(new Dog('puppy'), new Cat('kitten'));
hybrid1.bark();
hybrid1.meow();


// Mixins
function applyMixins(derivedCtor: any, baseCtors: any[]) {
    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            derivedCtor.prototype[name] = baseCtor.prototype[name];
        });
    });
}

class DogCatHybrid extends Animal implements Dog, Cat {
    constructor(public name: string) {
        super(name);
    }

    bark: () => void; // empty decalration
    meow: () => void; // empty decalration
}
applyMixins(DogCatHybrid, [Dog, Cat]);

let hybrid2: DogCatHybrid = new DogCatHybrid('Freaking hybrid');
hybrid2.bark();
hybrid2.meow();


// Union types
let c1: Dog | Cat = new Dog();
///let c2: Dog | Hamster = new Cat();


// Type guards
function hamsterGuard(input: any): input is Hamster {
    return input.boo !== undefined;
}

function hybridOr(input: Dog | Cat | Hamster): void {
    // common prop available here
    let name = input.name;

    if (input instanceof Dog) {
        input.bark();
    } else if (hamsterGuard(input)) {
        input.boo();
    } else {
        input.meow();
    }
}

hybridOr(new Dog('dog'));
hybridOr(new Cat('cat'));

